/*
 * TestPipe
 * KHR DEB 2014
 */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "pthread.h"

#define MAXSTRLENGTH 100

int main(int argc, char *argv[])
{
    int pipePtoC[2]; /* parent to child */
    int pipeCtoP[2]; /* child to parent */
    char mystring[MAXSTRLENGTH];
    char mystring2[MAXSTRLENGTH];
    char mystring3[MAXSTRLENGTH];

    /*char *pargs[3];
    char arg1[MAXSTRLENGTH];
    char arg2[MAXSTRLENGTH];
    char arg3[MAXSTRLENGTH];
    strcpy(arg1, "");
    pargs[0] = arg1;
    pargs[1] = arg2;
    pargs[2] = arg3;*/

    pipe (pipePtoC);
    pipe (pipeCtoP);

    pid_t pid = fork();

    if (pid == 0) { /* Child */
        close(pipePtoC[1]);

        strcpy(mystring, "/bin/myproc.elf");
        sprintf(mystring2, "%d", pipePtoC[0]);
        sprintf(mystring3, "%d", pipeCtoP[1]);

        printf("%s, %s\n", mystring2, mystring3);
    }
    else { /* Parent */
        close(pipeCtoP[1]);

        sprintf(mystring2, "%d", pipeCtoP[0]);
        sprintf(mystring3, "%d", pipePtoC[1]);
        strcpy(mystring, "/bin/myproc.elf");

        printf("%s, %s\n", mystring2, mystring3);
    }

    return 0;
}
