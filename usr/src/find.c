/*
 * ls.c
 *
 *  Created on: 24 novembre 2010
 *      Author: Lionel sambuc
 */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "dirent.h"


#define TRUE (1)
#define FALSE (0)
#define MAX_PATH 1024

typedef struct option {
	char active;
	union {
		char* name;
		unsigned int type;
		unsigned int size;
	} value;
} option_t;

#define OPTIONS_NAME_SIZE 5
enum options_name {SNAME, STYPE, SSIZEEQ, SSIZELT, SSIZEGT};
enum options_type {SFILE, SDIR};

/**
 * Print the usage help.
 */
void usage(void)
{
	printf( "Usage: find path [-name name] [-type type] [-size s]\n"
			"\t\t  [-sizelt s] [-sizegt s]\n"
			"\tAt least one option must be present.\n"
			"\tname\tthe name to search for, it will match directories\n"
			"\t\t or files. No wildcards allowed.\n");
	printf(	"\ttype\teither f for file or d for directory\n"
			"\tsize\tthe file has exactly the size s\n"
			"\tsizegt\tthe file size is greater than s\n"
			"\tsizelt\tthe file size is less than s\n");

}

/**
 * Parses the command line and set up the search options.
 * Errors :
 *  -1 if not enough parameters
 *  -2 if path is not a valid directory
 *  -3 if name is not a valid name
 *  -4 if type is not one of { f, F, d, D }
 *  -5 if size is not a valid integer
 */
int parse_options(int argc, char **argv, option_t *opts)
{
	int i = 0;

	/* Minimal syntax : find path -opt val
	 * This implies at minimum 4 arguments. */
	if (argc < 4)
		return -1;

	/* Move into the given base directory. The search is then based on
	 * the current directory.*/
	if (chdir(argv[1]) != 0)
		return -2;

	while ((i + 2 < argc) && (i + 3 < argc))
	{
		i += 2;

		if (strcmp("-name", argv[i]) == 0)
		{
			if (strlen(argv[i + 1]) == 0)
				return -3;

			/* FIXME: Check name correctness */
			opts[SNAME].value.name = argv[i+1];
			opts[SNAME].active = TRUE;
			continue;
		}

		if (strcmp("-type", argv[i]) == 0)
		{
			if ((strcmp("f", argv[i+1]) == 0) || (strcmp("F", argv[i+1]) == 0))
			{
				opts[STYPE].value.type = SFILE;
				opts[STYPE].active = TRUE;
				continue;
			}

			if ((strcmp("d", argv[i+1]) == 0) || (strcmp("D", argv[i+1]) == 0))
			{
				opts[STYPE].value.type = SDIR;
				opts[STYPE].active = TRUE;
				continue;
			}

			return -4;
		}

		if (strcmp("-size", argv[i]) == 0)
		{
			char *n = argv[i+1];
			if(!(*n >= '0' && *n <= '9'))
				return -5;
			opts[SSIZEEQ].active = TRUE;
			opts[SSIZEEQ].value.size = atoi(argv[i+1]);
		}

		if (strcmp("-sizelt", argv[i]) == 0)
		{
			char *n = argv[i+1];
			if(!(*n >= '0' && *n <= '9'))
				return -5;
			opts[SSIZELT].active = TRUE;
			opts[SSIZELT].value.size = atoi(argv[i+1]);
		}

		if (strcmp("-sizegt", argv[i]) == 0)
		{
			char *n = argv[i+1];
			if(!(*n >= '0' && *n <= '9'))
				return -5;
			opts[SSIZEGT].active = TRUE;
			opts[SSIZEGT].value.size = atoi(argv[i+1]);
		}

	}

	return 0;
}

int check_item(char *name, option_t *opts)
{
	int threshold = 0;
	int result = 0;
	int i = 0, ret = 0;
	struct stat infos = {};

	/* The threshold is the minimum value a result must have to be a match.
	 * This is simply the sum (AND) of the given options. */
	for (i = 0; i < OPTIONS_NAME_SIZE; i++)
	{
		threshold += (opts[i].active == TRUE ? 1 : 0);
	}

	ret = stat(name, &infos);
	if (ret < 0)
		printf("Could not get the status on file %s\n", name);

	if ((opts[SNAME].active == TRUE) && (strcmp(infos.st_name, opts[SNAME].value.name) == 0))
		result++;

	if (opts[STYPE].active == TRUE)
	{
		switch (opts[STYPE].value.type)
		{
		case SFILE:
			if (infos.st_flags != 0)
				result++; /* We expected a file, and it is one. */
			break;
		case SDIR:
			if (infos.st_flags == 0)
				result++; /* We expected a directory, and it is one. */
			break;
		default:
			printf("Unknown option.\n");
			return -1;
		}
	}

	if ((opts[SSIZEEQ].active == TRUE) && (infos.st_size == opts[SSIZEEQ].value.size))
		result++;

	if ((opts[SSIZELT].active == TRUE) && (infos.st_size < opts[SSIZELT].value.size))
		result++;

	if ((opts[SSIZEGT].active == TRUE) && (infos.st_size > opts[SSIZEGT].value.size))
		result++;

	return (result == threshold) ? TRUE : FALSE;
}

/**
 * Browse the given subtree for entries matching the search criteria.
 * Returns the number of files found or a negative error code.
 */
int search(option_t *opts)
{
	int ret = 0;
	char cwd[MAX_PATH] = {};

	/* This is use to browse the hierarchy. */
	DIR *dir_stream = NULL;
	struct dirent dir_entry = {};
	struct dirent* p_entry = NULL;

	/* This is used to communicate between processes. */
	int pipefd[2] = {-1, -1};
	int pid = -1, status = -1;
	int count = 0; /* Total number of files found. */

	dir_stream = opendir(".");
	if (dir_stream < 0)
	{
		ret = errno;
		printf("Could not open directory %s, errno %d\n", getcwd(cwd, MAX_PATH), ret);
		return ret;
	}

	while ((p_entry = readdir(dir_stream, &dir_entry)) != NULL)
	{
		/* 1/ If it is a directory, we must then fork and wait for the results.
		 * 2/ For a file or a parsed directory, we check if it corresponds to
		 *    the searched items. */
		if (p_entry->isDirectory != 0)
		{
			ret = pipe(pipefd);
			if (ret < 0)
			{
				ret = errno;
				printf("Could not create pipe, errno %d\n", ret);
				close((FILE) dir_stream);/* In Nachos, file and directory descriptor are the same. */
				return ret;
			}

			pid = fork();
			if (pid < 0)
			{
				ret = errno;
				printf("Could not fork, errno %d\n", ret);
				close((FILE) dir_stream);
				return ret;
			}

			if (pid == 0) /* We are the Son */
			{
				close(pipefd[0]); /* Close read end. */
				close((FILE) dir_stream);

				ret = chdir(p_entry->d_name);
				if (ret != 0)
				{
					printf("Could not set working directory to %s/%s\n", getcwd(cwd, MAX_PATH), p_entry->d_name);
					close(pipefd[1]); /* Close write end. */
					exit(-1);
				}

				/* Begin a new search from the new current directory. */
				ret = search(opts);

				if (ret < 0)
				{
					printf("Error during the search in %s/%s %d\n", getcwd(cwd, MAX_PATH), p_entry->d_name, ret);
					close(pipefd[1]); /* Close write end. */
					exit(ret);
				}

				ret = write(pipefd[1], &ret, 4);
				if (ret < 0)
				{
					printf("Could not write into the pipe.\n");
					close(pipefd[1]); /* Close write end. */
					exit(ret);
				}

				close(pipefd[1]); /* Close write end. */

				exit(0);
			}
			else /* We are the Father */
			{
				close(pipefd[1]); /* Close write end. */
				ret = waitpid(pid, &status, 0);
				if (ret != 1)
				{
					printf("Child process died unexpectedly\n");
					close(pipefd[0]); /* Close read end. */
					continue;
				}

				ret = read(pipefd[0], &status, 4);
				if (ret != 4)
				{
					printf("Error while reading pipe.\n");
					close(pipefd[0]); /* Close read end. */
					continue;
				}

				count += status;
				close(pipefd[0]); /* Close read end. */
			}
		}

		/* Check the file / directory. */
		ret = check_item(p_entry->d_name, opts);
		if (TRUE == ret)
		{
			count++;
			if (strcmp("/", getcwd(cwd, MAX_PATH)) == 0)
				printf("/%s\n", p_entry->d_name);
			else
				printf("%s/%s\n", cwd, p_entry->d_name);
		}
	}

	close((FILE) dir_stream);

	return count;
}

/**
 * Main entry, first checks the arguments, then start the search and
 * finally print the number of files found.
 */
int main(int argc, char **argv) {
	option_t options[sizeof(enum options_name)] = {};
	int ret = 0;

	ret = parse_options(argc, argv, options);
	if (ret < 0)
	{
		char *msg;
		switch(ret)
		{
		case -1: msg = "Arguments missing"; break;
		case -2: msg = "path is not a valid directory"; break;
		case -3: msg = "name is invalid"; break;
		case -4: msg = "type is invalid, must be one of {f, F, d, D}"; break;
		case -5: msg = "invalid size, not a number >= 0"; break;
		default: msg = "Unknown error during argument parsing"; break;
		}

		printf("ERROR: %s\n\n", msg);
		usage();
		exit(ret);
	}

	ret = search(options);

	printf("Found %d results\n", ret);
	return ret;
}
