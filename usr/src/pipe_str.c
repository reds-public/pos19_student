/*
 * Copyright (C) 2014-2017 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>

#define STR_SIZE	80

int main(int argc, char *argv[])
{
    int pipe_p_to_c[2]; /* Pipe Parent-to-Child */
    char buf[STR_SIZE];
    int i, ret;

    pipe (pipe_p_to_c);

    pid_t pid = fork();

    if (pid == 0) { /* Child */
        close(pipe_p_to_c[1]);

        printf("Child got: ");
        while ((ret = read(pipe_p_to_c[0], buf, STR_SIZE)) > 0)
        	printf("%s", buf);

        printf("\n");

        close(pipe_p_to_c[0]);

    }
    else { /* Parent */

	close(pipe_p_to_c[0]);

	for (i = 0; i < argc-1; i++) {
		write(pipe_p_to_c[1], argv[i+1], strlen(argv[i+1]));
		write(pipe_p_to_c[1], " ", 1);
	}
	write(pipe_p_to_c[1], "\0", 1);

        close(pipe_p_to_c[1]);

        waitpid(pid, NULL, 0);
    }

    return 0;
}
