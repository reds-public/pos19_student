/*
 * Copyright (C) 2014-2017 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2017-2018 Xavier Ruppen <xavier.ruppen@heig-vd.ch>
 * Copyright (C) 2017 Alexandre Malki <alexandre.malki@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <stdlib.h>

#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>


//#define __LINUX__

#ifdef __LINUX__
#include <sys/wait.h>

#endif

#define BUFSIZE 256

void do_trace(pid_t child)
{
	int status;
#ifdef __LINUX__
	struct user_regs_struct regs;
#else
	struct user regs;
#endif
	/* First synchronization with the tracee */
	waitpid(child, &status, 0);
	if (WIFEXITED(status))
		return ;

	/* Trace each syscall executed by the tracee and displays the syscall number
	 * and the return value.
	 */
	while (1) {

		ptrace(PTRACE_SYSCALL, child, NULL, NULL);

		waitpid(child, &status, 0);

		if (WIFEXITED(status))
			return ;

		ptrace(PTRACE_GETREGS, child, NULL, &regs);

#ifdef __LINUX__
		printf("syscall : %d\n", regs.orig_rax);
#else
		printf("syscall : %d\n", regs.regs.uregs[7]);
#endif

		ptrace(PTRACE_SYSCALL, child, NULL, NULL);

		/* Syscall exit - we might recover the return value */

		waitpid(child, &status, 0);
		if (WIFEXITED(status))
			return ;

		ptrace(PTRACE_GETREGS, child, NULL, &regs);

#ifdef __LINUX__
		printf("ret: %x\n", regs.rax);
#else
		printf("ret: %x\n", regs.regs.uregs[0]);
#endif

	}
}


void do_child(int argc, char **argv)
{
	char prog[BUFSIZE];
	int ret;

#ifdef __LINUX__
	strcpy(prog, argv[0]);
#else
	strcpy(prog, argv[0]);
	strcat(prog, ".elf");
#endif

	ptrace(PTRACE_TRACEME, 0, NULL, NULL);

	ret = execv(prog, argv);
	printf(" ret = %d\n", ret);
	if (ret < 0) {
		printf("### failed to exec %s\n", argv[0]);
		exit(-1);
	}

}

int main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s prog args\n", argv[0]);
		return 1;
	}

	pid_t child = fork();

	if (child == 0) /* Tracee */
		do_child(argc-1, &argv[1]);
	else /* Tracer */
		do_trace(child);

	return 0;
}
