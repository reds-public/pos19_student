/**
 * Shell program - REDS Institute, HEIG-VD
 * Modif. 2015
 */

#include <unistd.h>
#include <stdio.h>

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>


#include <dirent.h>
#include <syscall.h>

#define BUFFERSIZE	1024

#define MAXARGSIZE	16
#define MAXARGS		16

/* History */

/* Maximal number of entries in the history
 * Add an extra cell to store the EMPTY separator.
 * The EMPTY separator shows where the end of the history is if the history is full. */
#define MAXHISTO        11
/* Maximal number of characters in a command stored in the history (incl. final \0) */
#define MAXCMDLENGTH    101

static char history[MAXHISTO][MAXCMDLENGTH]; /* Array of values */
static int history_i; /* Circular buffer counter */
static int history_j; /* Counter for history printing */

/* Environment variables */

/*
 * Maximal number of environment variables
 */
#define MAXENV 10
/* Maximal length of the name of a variable and its content (incl. final \0) */
#define MAXSTRLENGTH 101

/* Structure that contains the name of an environment variable and its content */
typedef struct {
	char name[MAXSTRLENGTH];
	char content[MAXSTRLENGTH];
} envvar;

static envvar envvartab[MAXENV]; /* Array of environment variables */
static int envvar_i; /* Circular buffer counter */



/* Circular buffer functions */

/* Forward direction: returns the index of the next cell */
int next_circ(int i, int max)
{
	return ((i + 1) % max);
}

/* Backward direction: returns the index of the previous cell */
int prev_circ(int i, int max)
{
	return ((i - 1) % max);
}

/* History functions */

/* Print the content of the history */
void print_history()
{
	int i;

	for (i = next_circ(history_i, MAXHISTO) ; i != history_i ; i = next_circ(i, MAXHISTO))
		if (strlen(history[i]) > 0)
			printf("%s\n", history[i]);
}

/* Print the content of the history circular buffer */
void print_history_buffer()
{
	int i;

	for (i = 0 ; i < MAXHISTO ; i++) {
		if (strlen(history[i]) > 0)
			printf("%s", history[i]);
		else
			printf("EMPTY");
		if (i == history_i)
			printf(" *");
		printf("\n");
	}
}
/* Print the environment variables */
void print_env_vars()
{
	int i;

	for (i = 0 ; i < MAXENV ; i++)
		if (strlen(envvartab[i].name) > 0)
			printf("%s=%s\n", envvartab[i].name, envvartab[i].content);
}

/**
 * tokenizeCommand
 *
 * Split the specified command line into tokens, creating a token array with a maximum
 * of maxTokens entries, using storage to hold the tokens. The storage array should be as
 * long as the command line.
 *
 * Whitespace (spaces, tabs, newlines) separate tokens, unless
 * enclosed in double quotes. Any character can be quoted by preceeding
 * it with a backslash. Quotes must be terminated.
 *
 * Return the number of tokens, or -1 on error.
 */
static int tokenizeCommand(char *command, int maxTokens, char *tokens[], char *storage) {
	const int quotingCharacter = 0x00000001;
	const int quotingString = 0x00000002;
	const int startedArg = 0x00000004;

	int state = 0;
	int numTokens = 0;

	char c;

	assert(maxTokens> 0);

	while ((c = *(command++)) != '\0') {
		if ((c == 3) || (c == 10) || (c == 13))
			c = 'Z';
		if (state & quotingCharacter) {
			switch (c) {
			case 't':
				c = '\t';
				break;
			case 'n':
				c = '\n';
				break;
			}
			*(storage++) = c;
			state &= ~quotingCharacter;
		}
		else if (state & quotingString) {
			switch (c) {
			case '\\':
				state |= quotingCharacter;
				break;
			case '"':
				state &= ~quotingString;
				break;
			default:
				*(storage++) = c;
				break;
			}
		}
		else {
			switch (c) {
			case ' ':
			case '\t':
			case '\n':
				if (state & startedArg) {
					*(storage++) = '\0';
					state &= ~startedArg;
				}
				break;
			default:
				if (!(state & startedArg)) {
					if (numTokens == maxTokens)
						return -1;
					tokens[numTokens++] = storage;
					state |= startedArg;
				}

				switch (c) {
				case '\\':
				state |= quotingCharacter;
				break;
				case '"':
				state |= quotingString;
				break;
				default:
					*(storage++) = c;
					break;
				}
				break;
			}
		}
	}

	if (state & quotingCharacter) {
		printf("Unmatched \\.\n");
		return -1;
	}

	if (state & quotingString) {
		printf("Unmatched \".\n");
		return -1;
	}

	if (state & startedArg)
		*(storage++) = '\0';

	return numTokens;
}


/*
 * Parse and execute the line
 */
void runline(char *line)
{
	int background, status;
	int pid0;
	int i, j;
	int argc, argc2;
	int ret = 0;

	char args[BUFFERSIZE], args2[BUFFERSIZE], prog[BUFFERSIZE], cwd[BUFFERSIZE];
	char *argv[MAXARGS], *argv2[MAXARGS];

	char cmd1[BUFFERSIZE], cmd2[BUFFERSIZE];
	int redirectOn = 0;

	/* Substrings for the environment variables */
	char envvarname[MAXSTRLENGTH];
	char envvarcontent[MAXSTRLENGTH];

	char substrafterdollar[MAXSTRLENGTH];
	char substrafterspace[MAXSTRLENGTH];
	char left[MAXSTRLENGTH];
	char right[MAXSTRLENGTH];

	char *dollarindex; /* Index of the '$' */
	char *spaceindex; /* Index of the ' ' after the variable name */

	/* If a '$' is found, look for the environment variable */
	while ((dollarindex = strchr(line, '$')) != NULL) {
		strncpy(left, line, dollarindex - line);
		/* Add the \0 to the end of the string as strncpy does not do this */
		left[dollarindex - line] = '\0';
		strncpy(substrafterdollar, dollarindex + 1, strlen(line) - strlen(left) - 1);
		substrafterdollar[strlen(line) - strlen(left) - 1] = '\0';

		if ((spaceindex = strchr(substrafterdollar, ' ')) != NULL) {
			strncpy(envvarname, substrafterdollar, spaceindex - substrafterdollar);
			envvarname[spaceindex - substrafterdollar] = '\0';
			strncpy(substrafterspace, spaceindex + 1, strlen(substrafterdollar) - strlen(envvarname) - 1);
			substrafterspace[strlen(substrafterdollar) - strlen(envvarname) - 1] = '\0';
		}
		else { /* The variable is at the end of the line */
			strcpy(envvarname, substrafterdollar);
			strcpy(substrafterspace, "");
		}

		/* Look for the environment variable in the buffer */
		strcpy(envvarcontent, "");
		for (i = 0 ; i < MAXENV ; i++)
			if (strcmp(envvartab[i].name, envvarname) == 0)
				strcpy(envvarcontent, envvartab[i].content);

		strcpy(right, envvarcontent);
		strcat(right, " ");
		strcat(right, substrafterspace);

		strcpy(line, left);
		strcat(line, right);
	}

	/* Look for '>' (redirections) */

	strcpy(cmd1, "");
	strcpy(cmd2, "");

	/* Left side (or whole line if nothing is found) */
	for (i = 0 ; ((!redirectOn) && (i < strlen(line) + 1)) ; i++)
		switch (line[i]) {
		case '>':
			cmd1[i] = 0;
			redirectOn = 1;
			break;

		default:
			cmd1[i] = line[i];
			break;
		}

	/* Right side (if something has been found) */
	i = strlen(cmd1);
	if (i < strlen(line)+1)
		for (j = 0, i++ ; i < strlen(line)+1; i++, j++)
			cmd2[j] = line[i];

	argc = tokenizeCommand(cmd1, MAXARGS, argv, args);
	if (argc <= 0)
		return;

	if (*cmd2) {

		argc2 = tokenizeCommand(cmd2, MAXARGS, argv2, args2);

		if (argc2 <= 0)
			return;
	}

	if (argc > 0 && strcmp(argv[argc - 1], "&") == 0) {
		argc--;
		background = 1;
	}
	else
		background = 0;

	if (argc > 0) {

		if (strcmp(argv[0], "exit") == 0) {
			if (argc == 1)
				exit(0);
			else if (argc == 2)
				exit(atoi(argv[1]));
			else {
				printf("exit: Expression Syntax.\n");
				return;
			}
		}
		else if (strcmp(argv[0], "halt") == 0) {
			if (argc == 1) {
				halt();
				printf("Not the root process!\n");
			}
			else
				printf("halt: Expression Syntax.\n");

			return;
		}
		else if (strcmp(argv[0], "cd") == 0) {
			if (argc > 1) {
				chdir(argv[1]);
				if (errno == ENOENT)
					printf("failed\n");
			}
			return;
		}
		else if (strcmp(argv[0], "mkdir") == 0) {
			if (argc > 1) {
				mkdir(argv[1], 0777);
				if (errno == EACCES)
					printf("Permission denied\n");
				else if (errno)
					printf("Failed\n");
			}
			return;
		}
		else if (strcmp(argv[0], "pwd") == 0) {
			getcwd(cwd, BUFFERSIZE);

			printf("%s\n", cwd);
			return;
		}
		else if (strcmp(argv[0], "waitpid") == 0) {
			if (argc == 2)
				pid0 = atoi(argv[1]);
			else {
				printf("waitpid: Expression Syntax.\n");
				printf("usage: waitpid <pid>\n");
				return;
			}
		}
		else if (strcmp(argv[0], "dumpheap") == 0) {
			sysinfo(SYSINFO_DUMP_HEAP, 0);
			return ;
		}
		else if (strcmp(argv[0], "dumpzombie") == 0) {
			sysinfo(SYSINFO_DUMP_ZOMBIE, 0);
			return ;
		}
		else if (strcmp(argv[0], "testmalloc") == 0) {
			sysinfo(SYSINFO_TEST_MALLOC, atoi(argv[1]));
			return ;
		}
		/* Show the circular buffer content */
		else if (strcmp(argv[0], "history_buffer") == 0) {
			print_history_buffer();

			return;
		}
		/* List the commands stored in the history */
		else if (strcmp(argv[0], "history") == 0) {
			print_history();

			return;
		}
		/* Show the environment variables */
		else if (strcmp(argv[0], "env") == 0) {
			print_env_vars();

			return;
		}
		else { /* All the other commands */

			pid0 = fork();
			if (!pid0) {


					strcpy(prog, argv[0]);
					strcat(prog, ".elf");

					if (exec(prog, argc, argv) == -1) {
						printf("%s: exec failed.\n", argv[0]);
						exit(-1);
					}
			}
		}

		if (!background) {

			ret = waitpid(pid0, &status, 0);

			if (ret == -1)
				printf("waitpid (pid): Invalid process ID.\n");
			else if (ret == 0)
				printf("\n[%d] Unhandled exception\n", pid0);
			else
				printf("\n[%d] Done (%d)\n", pid0, WEXITSTATUS(status));
		}
	}
}


/* Clear the command line and prints the new command */
int changecommand(char *buffer, char *newcommand)
{
	int i;
	int ncharprint; /* Number of printed chars on the console */

	ncharprint = strlen(buffer);

	/* Clear the command line */
	for (i = 0 ; i < ncharprint ; i++)
		printf("\b \b");

	ncharprint = strlen(newcommand); /* Count the number of chars to print */
	printf("%s", newcommand); /* Print the command */
	strcpy(buffer, newcommand); /* Copy the command to the buffer */

	return ncharprint;
}

/* Get the command from the user console, giving the possibility to edit the input line. */
void getcommand(char *s, int maxlength) {
	int i = 0;      /* Position of the cursor */
	int length = 0; /* Command length */
	int delta = 0;  /* Distance between the position of the cursor and the length of the command */
	int cnt, cnt2;
	char buffer[BUFFERSIZE]; /* Buffer for the characters after the position of the cursor, used when a line is refreshed */

	while (1) {
		char c = getchar(); /* Get one character from the console */

		switch(c) {
		/* Backspace */
		case '\b':
		case 127:
			/* If there is nothing to delete, beep... */
			if (i == 0) {
				//beep();
			}
			/* ...else if the cursor is at the end of the line, delete the character... */
			else if (i == length) {
				printf("\b \b");
				fflush(stdout);
				i--;
				length--;
			}
			/* ...else delete the character before the cursor and refresh the end of the line */
			else {
				/* Delete everything until the end of the line is reached */
				for (cnt = 0 ; cnt < delta + 1 ; cnt++)
					printf("\b \b");
				i--;
				length--; /* The line looses one character */

				/* Refresh the end of the line */
				cnt = i;
				cnt2 = delta;
				while (cnt < length) {
					s[cnt] = buffer[--cnt2];
					putchar(s[cnt++]);
				}
				/* Move the cursor to the right position */
				cnt = i;
				while (cnt < length) {
					putchar(1);
					cnt++;
				}
			}
			break;
			/* Line feed, carriage return */
		case '\n':
		case '\r':
			putchar('\n');
			i = length;
			s[i] = 0;

			if (i > 0) {
				strcpy(history[history_i], s);
				history_i = next_circ(history_i, MAXHISTO);
				strcpy(history[history_i], "");

				history_j = history_i;
			}

			return;
			/* CTRL+C */
		case 3:
			s[0] = c;
			s[1] = 0;
			return; /* Return now */
			/* Up arrow */
		case 4:
			if (strlen(history[prev_circ(history_j, MAXHISTO)]) > 0) {
				history_j = prev_circ(history_j, MAXHISTO);

				length = changecommand(s, history[history_j]);

				i = length;
				delta = 0;
			}
			break;
			/* Down arrow */
		case 5:
			/* Avoid printing the first command twice */
			if (strlen(history[prev_circ(history_j, MAXHISTO)]) == 0)
				history_j = next_circ(history_j, MAXHISTO);

			if (strlen(history[history_j]) > 0) {
				length = changecommand(s, history[history_j]);

				history_j = next_circ(history_j, MAXHISTO);
			}
			else
				length = changecommand(s, "");

			i = length;
			delta = 0;

			break;
			/* Left arrow */
		case 6:
			/* Go to the left */
			if ((length > 0) && (i > 0)) {
				buffer[delta++] = s[--i];
				putchar(1); /* Make the cursor go to the left */
			}
			break;
			/* Right arrow */
		case 7:
			/* Go to the right */
			if ((length > 0) && (delta > 0)) {
				i++;
				delta--;
				putchar(2); /* Make the cursor go to the right */
			}
			break;
			/* Delete key */
		case 126:
			/* If we are at the end of the line, there is nothing to delete */
			if (i == length) {
				//beep();
				continue;
			}

			/* Delete everything until the end of the line is reached */
			for (cnt = 0 ; cnt < delta ; cnt++)
				printf("\b \b");
			length--; /* The line looses one character */
			delta--;

			/* Refresh the end of the line */
			cnt = i;
			cnt2 = delta;
			while (cnt < length) {
				s[cnt] = buffer[--cnt2];
				putchar(s[cnt++]);
			}
			/* Move the cursor to the right position */
			cnt = i;
			while (cnt < length) {
				putchar(1);
				cnt++;
			}
			break;
		default:
			/* If this is a bad character or if there is no room for more, beep... */
			if ((c < 0x20) || (i+1 == maxlength)) {
				//beep();
			}
			/* ...else add the character */
			else {
				/* Standard case: the cursor is on the last character */
				if (i == length) {
					length++;
					s[i++] = c;
					putchar(c);
				}
				/* Line editing: the cursor is somewhere else */
				else {
					/* Delete everything until the end of the line is reached */
					for (cnt = 0 ; cnt < delta ; cnt++)
						printf("\b \b");
					if (length + 1 < maxlength) {
						length++; /* one character is added */
						s[i++] = c; /* add the character */
						putchar(c); /* print the character */
					}

					/* Refresh the end of the line */
					cnt = i;
					cnt2 = delta;
					while (cnt < length) {
						s[cnt] = buffer[--cnt2];
						putchar(s[cnt++]);
					}
					/* Move the cursor to the right position */
					cnt = i;
					while (cnt < length) {
						putchar(1);
						cnt++;
					}
				}
			}
			break;
		}
	}
}

/*
 * MAIN
 */
void main(int argc, char *argv[])
{
	char prompt[] = "so3% ";

	char buffer[BUFFERSIZE]; /* Input buffer */
	//char buffer2[BUFFERSIZE]; /* Command from history */

	char firstchar;

	int i;

	/* History initialization */
	for (i = 0 ; i < MAXHISTO ; i++)
		strcpy(history[i], "");
	history_i = 0;

	/* Initialization of the evironment variables */
	for (i = 0 ; i < MAXENV ; i++) {
		strcpy(envvartab[i].name, "");
		strcpy(envvartab[i].content, "");
	}
	envvar_i = 0;

	/* Clear the input buffer */
	strcpy(buffer, "");

	while (1) {
		printf("%s", prompt);
		fflush(stdout);

		/* Input */
		getcommand(buffer, BUFFERSIZE);

		/* Get the first char to check if Ctrl+C has been sent */
		firstchar = buffer[0];

		/* Handle the first char */
		if (firstchar == '\3') { /* Ctrl-C */
			printf("\n");

			continue; /* Go to the beginning of the while loop */
		}

		/* Execute the line */
		runline(buffer);

		/* Clear the buffer */
		strcpy(buffer, "");
	}
}
