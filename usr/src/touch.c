#include "syscall.h"
#include "stdio.h"
#include "stdlib.h"

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: touch <file>\n");
		return 1;
	}

	int fd = open(argv[1], O_WRONLY);
	if (fd < 0){
		fd = creat(argv[1], 777);
		if (fd < 0) {
			printf("Unable to touch %s\n", argv[1]);
			return 1;
		}
	}

	close(fd);

	return 0;
}
