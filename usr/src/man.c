#include "syscall.h"
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include "string.h"

#define BUF_SZ	1024
#define STR_SZ 256
#define HEADER_PARAMS_SZ 5

enum {H_PGR_NAME=0, H_SECTION_NUMBER, H_CENTER_FOOTER, H_LEFT_FOOTER, H_CENTER_HEADER} header_params_t;

int console_height, console_width, offset = 0, line_cnt = 0, line_total = 0;
int indent=0, print_tag=0, inside_section = 0, newline_after = 0, indented_paragraph = 0;
char tmp[STR_SZ];


void rm_char(char* str, char c) {
    char *pr = str, *pw = str;
    while (*pr) {
        *pw = *pr++;
        pw += (*pw != c);
    }
    *pw = '\0';
}

void rm_tag(char *str, char *tag){
	char *pr = str, *pw = str;
	int offset = 0;

	while (*pr != '\0') {
		offset = 0;

		while (*(tag+offset) && *(pr+offset) == *(tag+offset)){
			offset++;
		}
		if (*(tag+offset) == '\0') {
			pr+=offset; //tag not fully detected so turn back
		}
		*pw++ = *pr++;

	}
	*pw = '\0';
}

void print_body(int lf_before, char * str){

	// if one or more newlines are requested
	while(lf_before-- > 0 || offset >= console_width){
		printf("\n");
		offset = 0;
		line_cnt++;
	}

	//if newline(s) occurred
	if (offset == 0) {
		if (inside_section){
			printf("\t");
			offset += 8;
		}
		indent = 1;
	}

	// if indent is requested
	if(indent){
		indent=0;
		if (indented_paragraph){
			printf("\t");
			offset += 8;
		}
	}

	// Print body text
	if (str != NULL){

		rm_tag(str, "\\fI");
		rm_tag(str, "\\fR");
		rm_tag(str, "\\fP");
		rm_tag(str, "\\fB");
		rm_char(str, '\\');
		size_t remaining = console_width-offset;
		//while (*str == ' ') str++;
		if (strlen(str) < remaining){ //one line
			printf("%s", str);
			offset+=strlen(str);
		}else{ //multiple lines
			strncpy(tmp, str, remaining);
			while (remaining > 0 && tmp[remaining-1] != ' ') remaining--;
			tmp[remaining] = '\0';
			printf("%s", tmp);
			print_body(1, str+remaining);
		}

		// if in middle of a line, to sep words
		if (offset != 0){
			printf(" ");
			offset++;
		}
	}
}

char* print_header_footer(const char *left, const char *center, const char *right, size_t width, char *header){

	char spaces_left[STR_SZ], spaces_right[STR_SZ];

	memset(spaces_left, ' ', width/2 - strlen(left) - strlen(center)/2);
	spaces_left[width/2 - strlen(left) - strlen(center)/2] = '\0';
	memset(spaces_right, ' ', width/2 - strlen(right) - strlen(center)/2);
	spaces_right[width/2 - strlen(right) - strlen(center)/2] = '\0';

	sprintf(header, "%s%s%s%s%s", left, spaces_left, center, spaces_right, right);

	return header;
}

int main(int argc, char **argv) {

	int err;
	FILE fd;
	char line[BUF_SZ], *pt_line = NULL;
	char tag[STR_SZ], *pt_tag = NULL;
	char pgr_section[STR_SZ];
	char params[HEADER_PARAMS_SZ][STR_SZ];
	int val[2];
	char key;

	if (argc != 2) {
		printf("Usage: man <page>\n");
		return 1;
	}

	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		char path[STR_SZ] = "/man/";
		fd = open(strcat(path, argv[1]), O_RDONLY);
		if (fd == -1) {
			printf("Error #%d:\n  Unable to open %s\n", errno, argv[1]);
			return 2;
		}
	}

	/* Parse header */

	do{ //eliminates possible comment tags .\"
		fgets(line, BUF_SZ, fd);// read a line
	}while (!strcmp(strncpy(tag, line, 4), ".\\\" "));

	if (!strcmp(tag, ".TH ")){ //start with .TH tag

		char *p, *start_of_word;
		int param_index = 0;
		int c;
		enum states { DULL, IN_WORD, IN_STRING } state = DULL;

		for (p = line+4; *p != 10 && *p != 0; p++) {
			c = (unsigned char) *p; /* convert to unsigned char for is* functions */

			switch (state) {
			case DULL: /* not in a word, not in a double quoted string */
				if (c==' ') {
					/* still not in a word, so ignore this char */
					continue;
				}
				/* not a space -- if it's a double quote we go to IN_STRING, else to IN_WORD */
				if (c == '"') {
					state = IN_STRING;
					start_of_word = p + 1; /* word starts at *next* char, not this one */
					continue;
				}
				state = IN_WORD;
				start_of_word = p; /* word starts here */
				continue;

			case IN_STRING:
				/* we're in a double quoted string, so keep going until we hit a close " */
				if (c == '"') {
					/* word goes from start_of_word to p-1 */

					strncpy(params[param_index], start_of_word, p-start_of_word);
					params[param_index++][p-start_of_word] = '\0';

					state = DULL; /* back to "not in word, not in string" state */
				}
				continue; /* either still IN_STRING or we handled the end above */

			case IN_WORD:
				/* we're in a word, so keep going until we get to a space */
				if (c == ' ') {
					/* word goes from start_of_word to p-1 */

					strncpy(params[param_index] ,start_of_word, p-start_of_word);
					params[param_index++][p-start_of_word] = '\0';

					state = DULL; /* back to "not in word, not in string" state */
				}
				continue; /* either still IN_WORD or we handled the end above */
			}
		}

		// if header is valid
		if (param_index == HEADER_PARAMS_SZ && atoi(params[1])) {

			err = ioctl(stdout, 1, val);
			if (err != 0) {
				printf("Error #%d\n  Ioctl error %d\n", errno, err);
				return 3;
			}
			console_height = val[0];
			console_width = val[1];


			sprintf(pgr_section, "%s(%s)", params[H_PGR_NAME], params[H_SECTION_NUMBER]);

			// Print header
			printf("%s", print_header_footer(pgr_section, params[H_CENTER_HEADER], pgr_section, console_width, line));

		}else{
			printf("Invalid header.\n");
			exit(1);
		}
	}else{
		printf("No header found.\n");
		exit(1);
	}

	/* Parse body */

	// read all lines in file
	line_cnt = 0; line_total = 0;
	while(fgets(line, BUF_SZ, fd) != NULL){
		pt_line = line;

		// It's an empty line --> new paragraph
		if (strlen(pt_line) == 0){
			print_body(2, NULL);
			continue;
		}
		// It's a tag
		else if (*pt_line == '.') {
			pt_tag = tag;

			while (*pt_line != ' ' && *pt_line != '\0') {
				*(pt_tag++) = *(pt_line++);
			}
			*pt_tag = '\0'; //terminate string

			// TAG with text following
			if (strlen(pt_line)) {
				while (*(++pt_line) == ' '); //trim spaces after tag
				if (!strcmp(tag, ".SH")){
					inside_section = 0;
					indented_paragraph = 0;
					indent=0;
					print_body(2, pt_line);
					inside_section = 1;
					print_body(1, NULL);

				}else if (!strcmp(tag, ".IP")){
					print_body(1, pt_line);
					indented_paragraph = 1;
					indent=1;
				}else if (!strcmp(tag, ".\\\"")){
					//do nothing
				}else{
					print_body(0, pt_line);
				}
			// TAG alone
			}else{
				if (!strcmp(tag, ".P") || !strcmp(tag, ".PP") || !strcmp(tag, ".TP")){
					print_body(2, NULL);
				}else if (!strcmp(tag, ".br")){
					print_body(1, NULL);
				}else if (!strcmp(tag, ".RS")){
					indented_paragraph = 1;
					indent=1;
					print_body(1, NULL);
				}else if (!strcmp(tag, ".RE")){
					//print_tag=1;
					indented_paragraph = 0;
					indent=1;
					print_body(1, NULL);
				}
			}
		// body text
		}else{
			print_body(0, pt_line);
		}

		//manage pages
		if (line_cnt >= (console_height - 3)) {
			line_total += line_cnt;
			line_cnt = 0;

			sprintf(pt_line, "\n -- Manual page %s line %d (press q to quit) --", argv[1], line_total);
			printf("%s", pt_line);

			key = getc(stderr);
			if ((key == 'q') || (key == 'Q')) exit(0);

			// don't let appear the line above after that
			int i;
			for (i=0; i<strlen(pt_line); i++){
				*(pt_line+i) ='\b';
			}
			*(pt_line+i) = '\0';
			printf("%s", pt_line);
		}
	};

	/* Print footer */
	printf("\n\n%s\n", print_header_footer(params[H_LEFT_FOOTER], params[H_CENTER_FOOTER], pgr_section, console_width, line));

	exit(0);
}
