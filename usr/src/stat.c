#include "stdio.h"
#include "syscall.h"
#include "dirent.h"

int main(int argc, char **argv) {
	struct stat info;
	int err = 0;

	/* Check arguments */
	if (argc != 2) {
		printf("Need two arguments, file name missing\n");
		return -1;
	}

	/* Stat function */
	if(stat(argv[1], &info) != 0)
	{
		err = errno;
		printf("stat failed: %d\n", err);
		exit(err);
	}

	/* Print  screen */
	printf("File name : %s\n", info.st_name);
	printf("File size : %d \n", info.st_size);
	printf("File last access : %s\n", info.st_time);

	if(info.st_flags == 1){
		printf("File is a regular file \n");
	} else {
		printf("File is not a regular file \n");
	}
	printf("Access mode : %c\n", info.st_mode);

	return 0;
}
