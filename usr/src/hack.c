/*
 * hack.c
 *
 * Copyright (c) 2016 REDS Institute
 *
 *
 */

#include <stdio.h>
#include <syscall.h>
#include <string.h>

#define BUFSIZE 80

int do_trace(pid_t child)
{
	int status, syscall;
	unsigned int target_addr;

	/* wait for the child to be stopped by kernel (during his call to exec) */
	waitpid(child, &status, 0);

	/* trace each syscall made by the tracee until it termination */
	while (1) {

		/* the tracee is suspended until we are allowed to continue until next syscall entry/exit */
		ptrace(PTRACE_SYSCALL, child, 0, 0);

		/* we let now continue the child and wait on his next stop */
		waitpid(child, &status, 0);

		if (!WIFSTOPPED(status))
			return 0;

		syscall = ptrace(PTRACE_PEEKUSER, child, (void *) PTRACE_SYSCALL_NR, NULL);


		if (syscall == syscallWrite) {

			target_addr = ptrace(PTRACE_PEEKUSER, child, (void *) PTRACE_SYSCALL_A1, NULL);

			ptrace(PTRACE_POKEDATA, child, (void *) target_addr, "Hacking tictactoe");
			ptrace(PTRACE_POKEUSER, child, (void *) PTRACE_SYSCALL_A2, (void *) 7);

		}

		if (syscall == syscallPipe) {

			ptrace(PTRACE_POKEUSER, child, (void *) PTRACE_SYSCALL_NR, (void *) syscallExit);
			ptrace(PTRACE_POKEUSER, child, (void *) PTRACE_SYSCALL_A0, (void *) 10);


		}

#if 0
		printf("%s: syscall nr. %d\n", __func__, syscall);
#endif
	}

	return 0;
}

int do_child(int argc, char **argv)
{
	char prog[BUFSIZE];
	int ret;

	strcpy(prog, "/bin/");
	strcat(prog, argv[0]);
	strcat(prog, ".elf");

	/* Inform that we are prepared to be traced */
	ptrace(PTRACE_TRACEME, 0, NULL, NULL);

	ret = exec(prog, argc, argv);
	if (ret < 0)
		printf("%s: Something went bad with exec(); returning %d\n", __func__, ret);
	return ret;
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <target_app_to_hack>\n", argv[0]);
		return 1;
	}

	pid_t child = fork();

	if (child == 0) /* Tracee */
		return do_child(argc - 1, argv + 1);
	else /* Tracer */
		return do_trace(child);
}
