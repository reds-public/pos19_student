/*
 * Copyright (C) 2014-2017 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2017-2018 Xavier Ruppen <xavier.ruppen@heig-vd.ch>
 * Copyright (C) 2017 Alexandre Malki <alexandre.malki@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define BUFFERSIZE 10

#define SECONDSINMINUTE 60
#define SECONDSINHOUR (SECONDSINMINUTE * 60)
#define SECONDSINDAY (SECONDSINHOUR * 24)
#define SECONDSINYEAR (SECONDSINDAY * 365)

/**
 * Returns the result of floor(n / d).
 * As we avoid using floating point numbers, a simple loop
 * counts how many times d is in n.
 */
int flr(int n, int d)
{
    int i;
    int result = 0;

    for (i = d ; i <= n ; i += d)
        result++;

    return result;
}

/**
 * Convert the number of elapsed seconds to the number of elapsed
 * years, days, hours, minutes and seconds
 */
void convert_time(int time,
                  int *years_sinceepoch,
                  int *days_sinceepoch,
                  int *hours_sinceepoch,
                  int *minutes_sinceepoch,
                  int *seconds_sinceepoch)
{
    *years_sinceepoch = flr(time, SECONDSINYEAR);
    time -= *years_sinceepoch * SECONDSINYEAR;
    *days_sinceepoch = flr(time, SECONDSINDAY);
    time -= *days_sinceepoch * SECONDSINDAY;
    *hours_sinceepoch = flr(time, SECONDSINHOUR);
    time -= *hours_sinceepoch * SECONDSINHOUR;
    *minutes_sinceepoch = flr(time, SECONDSINMINUTE);
    time -= *minutes_sinceepoch * SECONDSINMINUTE;
    *seconds_sinceepoch = time;
}

/**
 * Print the time
 */
void print_time()
{
    struct timeval tv;

    int years_sinceepoch,
        days_sinceepoch,
        hours_sinceepoch,
        minutes_sinceepoch,
        seconds_sinceepoch;

    gettimeofday(&tv);
    int time = tv.tv_sec;
    printf("Since epoch:\n%d s\n", time);
    convert_time(time,
                 &years_sinceepoch,
                 &days_sinceepoch,
                 &hours_sinceepoch,
                 &minutes_sinceepoch,
                 &seconds_sinceepoch);
    printf("%d y, %d d, %d h, %d m, %d s\n\n",
           years_sinceepoch,
           days_sinceepoch,
           hours_sinceepoch,
           minutes_sinceepoch,
           seconds_sinceepoch);
}

/**
 * Manual input
 * \param message: message to print
 * \param max: max admissible value (0 is the min admissible value)
 */
int input(char *message, int max)
{
    char buffer[BUFFERSIZE];
    strcpy(buffer, "");

    int ret;

    do {
        printf(message);
        gets(buffer, BUFFERSIZE);
        ret = atoi(buffer);
    }
    while ((ret < 0) || (ret > max));

    return ret;
}

/**
 * Wait by doing nothing interesting but looping...
 */
void wait_loop()
{
    int i, j;
    int h = 0;
    for (i = 0 ; i < 10000 ; i++) {
        for (j = 0 ; j < 300 ; j++)
            h++;
    }
}

/**
 *
 */
int main(int argc, char *argv[])
{
    struct timeval tv;

    print_time();

    /* Sets the time to November 10th, 00:00 */
    int wantedtime = 10;
    wantedtime *= 24;
    wantedtime *= 60;
    wantedtime *= 60;
    tv.tv_sec = wantedtime;
    settimeofday(&tv);

    /* Time before the loop */
    print_time();

    /* Waiting loop */
    wait_loop();

    /* Time after the loop */
    print_time();

    /* Manual input */
    wantedtime = input("Seconds: ", 59);
    wantedtime += input("Minutes: ", 59) * SECONDSINMINUTE;
    wantedtime += input("Hours: ", 23) * SECONDSINHOUR;
    wantedtime += input("Days: ", 364) * SECONDSINDAY;
    wantedtime += input("Years: ", 10) * SECONDSINYEAR;
    tv.tv_sec = wantedtime;
    settimeofday(&tv);

    /* Time after manual set */
    print_time();

    /* Waiting loop */
    wait_loop();

    /* Time after the loop */
    print_time();

    return 0;
}
