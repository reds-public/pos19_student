
#!/bin/bash

usage()
{
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "Where OPTIONS are:"
  echo "  -a    Deploy all"
  echo "  -b    Deploy boot (kernel, U-boot, etc.)"
  echo "  -r    Deploy rootfs (secondary)"
  echo ""
  exit 1
}

while getopts "abr" o; do
  case "$o" in
    a)
      deploy_rootfs=y
      deploy_boot=y
      deploy_usr=y
      ;;
    b)
      deploy_boot=y
      ;;
    r) 
      deploy_rootfs=y
      ;;
    *)
      usage
      ;;
  esac
done

if [ $OPTIND -eq 1 ]; then usage; fi

while read var; do
if [ "$var" != "" ]; then
  export $(echo $var | sed -e 's/ //g' -e /^$/d -e 's/://g' -e /^#/d)
fi
done < build.conf

if [ "$_PLATFORM" != "vexpress" ]; then
    echo "Specify the device name of MMC (ex: sdb or mmcblk0 or other...)" 
    read devname
    export devname="$devname"
fi

if [ "$deploy_boot" == "y" ]; then
    if [ "$_PLATFORM" == "vexpress" ]; then
        # Deploy files into the boot partition (first partition)
        echo Deploying boot files into the first partition...
        cd target
        ./mkuboot.sh ${_PLATFORM}
        cd ../filesystem
        ./mount.sh 1
        sudo rm -rf fs/*
        sudo cp ../target/vexpress.itb fs/
        sudo cp ../u-boot/uEnv.d/uEnv_vexpress.txt fs/uEnv.txt
        sudo ./umount.sh
        cd ..
    fi
    
    if [ "$_PLATFORM" == "lime2" ]; then
        # Deploy files into the boot partition (first partition)
        echo Deploying boot files into the first partition...
        cd target
        ./mkuboot.sh ${_PLATFORM}
        cd ../filesystem
        ./mount.sh 1
        sudo rm -rf fs/*
        sudo cp ../target/lime2.itb fs/
        sudo cp ../u-boot/uEnv.d/uEnv_lime2.txt fs/uEnv.txt
        sudo ./umount.sh
        cd ..
        sudo dd if=u-boot/u-boot-sunxi-with-spl.bin of=/dev/"$devname" bs=1024 seek=8
    fi
    
fi

if [ "$deploy_rootfs" == "y" ]; then
    # Deploy of the rootfs (second partition)
    cd rootfs
    ./deploy.sh
    cd ..
fi
    


