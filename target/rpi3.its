/*
 * Copyright (C) 2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/dts-v1/;

/ {
	description = "Components for Qemu/vExpress environment";
	#address-cells = <0>;
	#size-cells = <0>;

	images {
        #address-cells = <0>;
		#size-cells = <0>;
        
        avz {
		linux {
			description = "Linux kernel";
			data = /incbin/("../linux/arch/arm/boot/Image");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x00008000>;
			entry = <0x00008000>;
            
			#address-cells = <0>;
			#size-cells = <0>;

			hash {
				algo = "crc32";
			};
		};
			
       fdt {
			description = "Linux device tree blob";
			data = /incbin/("../linux/arch/arm/boot/dts/bcm2837-rpi-3-b.dtb");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			load = <0x0a000000>;
		
			hash {
				algo = "crc32";
			};
		};
	
		initrd {
			description = "Agency initial rootfs (initrd)";
			data = /incbin/("../rootfs/board/rpi3/initrd.cpio");
			type = "ramdisk";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x0b000000>;
			hash {
				algo = "crc32";
			};
		};
	};
	
	configurations {
		default = "conf";
		conf {
			description = "Linux on vExpress";
            kernel = "linux";
            fdt = "fdt";
            ramdisk = "initrd";
		};
	};
};
