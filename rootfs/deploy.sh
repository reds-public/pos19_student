#!/bin/bash

echo Deploying secondary rootfs into the second partition...
./mount_cpio.sh
cd ../filesystem
./mount.sh 2
sudo rm -rf fs/*
sudo cp -rf ../rootfs/cpio/* fs/
./umount.sh
cd ../rootfs
./umount_cpio.sh
