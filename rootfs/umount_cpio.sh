#!/bin/bash
cd cpio
truncate -s 0 ../rootfs.cpio
find . | cpio -o --format='newc' >> ../rootfs.cpio
cd ..
rm -rf cpio
