#!/bin/bash
cd initrd
truncate -s 0 ../board/$1/initrd.cpio
sudo chown -R root:root *
find . | cpio -o --format='newc' >> ../board/$1/initrd.cpio
cd ..
sudo rm -rf initrd
