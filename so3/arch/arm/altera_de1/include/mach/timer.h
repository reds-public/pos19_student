/*
 * Copyright (C) 2017 David Truan <david.truan@heig-vd.ch>
 * Copyright (C) 2017 Lucas Elisei <lucas.elisei@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */


#ifndef ARCH_ALTERA_DE1_TIMER_H_
#define ARCH_ALTERA_DE1_TIMER_H_

// Timers frequencies (First two at 100MHz, last two at 25MHz)
#define APB_TIMER01_FREQ	100000000ull
#define APB_TIMER23_FREQ	25000000ull

// Timers addresses
#define APB_TIMER0_BASE	0xFFC08000
#define APB_TIMER1_BASE	0xFFC09000
#define APB_TIMER2_BASE	0xFFD00000
#define APB_TIMER3_BASE	0xFFD01000

#endif /* ARCH_ALTERA_DE1_TIMER_H_ */