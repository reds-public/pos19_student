 /*
 * Copyright (C) 2017 Lucas Elisei <lucas.elisei@heig-vd.ch>
 * Copyright (C) 2017 David Truan <david.truan@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <common.h>

#include <asm/io.h>

#include <device/arch/dw_apb_uart.h>
#include <device/device.h>
#include <device/driver.h>
#include <device/serial.h>

#include <mach/uart.h>

static dw_apb_t *dw_apb_uart;

static int dw_apb_uart_put_byte(char c) {
	/* Wait until TX is empty. */
	while((dw_apb_uart->lsr & UART_LSR_THRE) == 0);

	/* Transform '\n' to "\n\r" */
	if (c == '\n') {
		dw_apb_uart->thr = c;
		dw_apb_uart_put_byte('\r');
	} else {
		dw_apb_uart->thr = c;
	}

	return 0;
}

static char dw_apb_uart_get_byte(bool polling) {
	/* Wait for data. */
	while((dw_apb_uart->lsr & UART_LSR_DR) == 0);

	return dw_apb_uart->rbr;
}

void __ll_put_byte(char c) {
	while((ioread32(UART_BASE + UART_LSR) & UART_LSR_THRE) == 0);

	if (c == '\n') {
		iowrite8(UART_BASE + UART_THR, '\n'); /* Line Feed */

		__ll_put_byte('\r');
	}
	else {
		iowrite8(UART_BASE + UART_THR, (uint8_t) c);
	}
}

static int dw_apb_uart_init(dev_t *dev) {
	DBG("UART initialization...\n");

	/* Initialize UART controller. */
	dw_apb_uart = (dw_apb_t *)dev->base;

	/* Set UART to 8n1 and disable interrupts. Baudrate configuration is already done by U-Boot. */
	iowrite32(&dw_apb_uart->lcr, UART_LCR_STOP_1 | UART_LCR_PDIS | UART_DL_8BITS);
	iowrite32(&dw_apb_uart->ier, 0x0);

	serial_ops.put_byte = dw_apb_uart_put_byte;
	serial_ops.get_byte = dw_apb_uart_get_byte;

	DBG("Finished UART initialization.\n");

	return 0;
}

REGISTER_DRIVER_CORE("serial,dw-apb-uart", dw_apb_uart_init);
