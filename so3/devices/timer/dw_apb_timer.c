/*
 * Copyright (C) 2017 David Truan <david.truan@heig-vd.ch>
 * Copyright (C) 2017 Lucas Elisei <lucas.elisei@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <common.h>
#include <schedule.h>
#include <softirq.h>
#include <types.h>

#include <asm/io.h>

#include <device/driver.h>
#include <device/irq.h>
#include <device/timer.h>
#include <timer.h>
#include <device/arch/dw_apb_timer.h>

#include <mach/timer.h>

#if 0
static dw_apb_timer_t *clocksource_dev;
#endif

static dw_apb_timer_t *periodic_dev;

static void dw_apb_periodic_start(void) {
	/* Convert nanoseconds to (seconds * TIMER_FREQ). */
	uint32_t period = (periodic_timer.period * APB_TIMER23_FREQ) / NSECS;

	DBG("periodic timer period set to: %u\n", period);

	/* Load period into register. */
	periodic_dev->load = period;

	/* Enable timer. */
	periodic_dev->control |= TIMER_CTRL_ENABLE;
}

static irq_return_t dw_apb_periodic_isr(int irq, void *dummy) {
	/* Clear interrupt. */
	ioread32(&periodic_dev->eoi);

	++jiffies;

	raise_softirq(SCHEDULE_SOFTIRQ);
	raise_softirq(TIMER_SOFTIRQ);

	return IRQ_COMPLETED;
}

static int dw_apb_periodic_init(dev_t *dev) {
	DBG("Periodic timer (0x%08X) initialization\n", dev->base);

	periodic_dev = (dw_apb_timer_t *)dev->base;

	/* Make sure timer is disabled. */
	periodic_dev->control &= ~TIMER_CTRL_ENABLE;

	/* Set periodic mode and enable interrupt. */
	periodic_dev->control = TIMER_CTRL_PERIODIC | TIMER_CTRL_IE;

	/* Configure interrupt routine. */
	irq_bind(dev->irq, dw_apb_periodic_isr, NULL, NULL);
	irq_ops.irq_enable(dev->irq);

	/* Configure generic periodic timer. */
	periodic_timer.dev = dev;
	periodic_timer.period = NSECS / HZ;
	periodic_timer.start = dw_apb_periodic_start;

	return 0;
}

#if 0 /* Not used for now, it needs to be apdapted to the new wrap mechanism */
static uint64_t dw_apb_clocksource_read(void) {
	/* Convert value to nanoseconds and add wrap value. */
	return (SECONDS(~(clocksource_dev->counter)) +
			(uint64_t)(clocksource_timer.nb_wrap * (uint64_t)(MAX_COUNTER_VALUE + 1))) / APB_TIMER23_FREQ;
}

static irq_return_t dw_apb_clocksource_isr(int irq) {
	/* Clear interrupt. */
	ioread32(&clocksource_dev->eoi);

	++(clocksource_timer.nb_wrap);

	return IRQ_COMPLETED;
}

static int dw_apb_clocksource_init(dev_t *dev) {
	DBG("Clocksource timer (0x%08X) initialization\n", dev->base);

	clocksource_dev = (dw_apb_timer_t *)dev->base;

	/* Make sure timer is disabled. */
	clocksource_dev->control &= ~TIMER_CTRL_ENABLE;

	/* Set free-running mode and enable interrupt. */
	clocksource_dev->control = TIMER_CTRL_FREE | TIMER_CTRL_IE;

	/* Load value to decrement from. */
	clocksource_dev->load = MAX_COUNTER_VALUE;

	/* Re-enable timer. */
	clocksource_dev->control |= TIMER_CTRL_ENABLE;

	/* Configure interrupt routine. */
	irq_bind(dev->irq, dw_apb_clocksource_isr, NULL, NULL);
	irq_enable(dev->irq);

	clocksource_timer.dev = dev;
	clocksource_timer.nb_wrap = 0;
	clocksource_timer.read = dw_apb_clocksource_read;

	return 0;
}
#endif

REGISTER_DRIVER_POSTCORE("timer2,dw-apb-timer", dw_apb_periodic_init);
