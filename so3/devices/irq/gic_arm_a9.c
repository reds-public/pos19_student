/**
 * This file contains the body of all functions which allow to initialize the
 * GIC and handle the interrupts.
 *
 * @author  David Truan
 * @date    29.11.2017
 *
 * @version 1.0
 */

#include <common.h>
#include <types.h>

#include <device/driver.h>
#include <device/irq.h>
#include <device/arch/gic_arm_a9.h>

#include <asm/io.h>
#if 1
#define DEBUG
#endif

static a9_gicc_t *a9_gicc_dev; /* represents all registers of the GIC CPU interface */
static a9_gicd_t *a9_gicd_dev; /* represents all registers of the GIC distributor */

/**
 * Configure the GIC distributor.
 */
static void a9_gicd_config(void) {
	unsigned int i;

	/* set all SPI interrupts to be level triggered, active low */
	for (i = 32; i < NB_IRQS; i += 16) {
		*(a9_gicd_dev->icfgr + (i / 16)) = GICD_INT_ACTLOW_LVLTRIG;
	}

	/* set priority on all SPI interrupts */
	for (i = 32; i < NB_IRQS; i += 4) {
		*(a9_gicd_dev->ipriorityr + (i / 4)) = GICD_INT_DEF_PRI_X4;
	}

	/* disable all SPI interrupts. Leave the PPI and SGIs alone
	   as they are enabled by redistributor registers */
	for (i = 32; i < NB_IRQS; i += 32) {
		*(a9_gicd_dev->icenabler + (i / 32)) = GICD_INT_EN_CLR_X32;
	}
}


/**
 * Configure the GIC CPU interface.
 */
static void a9_gicc_config(void) {
	unsigned int i;

	/* Disable PPI and enable SGI. */
	*(a9_gicd_dev->icenabler) = GICD_INT_EN_CLR_PPI;
	*(a9_gicd_dev->isenabler) = GICD_INT_EN_SET_SGI;

	/* Set priority on PPI and SGI interrupts. */
	for (i = 0; i < 32; i += 4) {
		*(a9_gicd_dev->ipriorityr + (i / 4)) = GICD_INT_DEF_PRI_X4;
	}
}

/**
 * Allows to mask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void a9_gicc_irq_mask(unsigned int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(a9_gicd_dev->icenabler + (irq / 32)) |= maskIrqID;
}

/**
 * Allows to unmask an interrupt.
 *
 * @param irq the interrupt ID
 */
static void a9_gicc_irq_unmask(unsigned  int irq) {
	uint32_t maskIrqID = 1 << (irq % 32);

	*(a9_gicd_dev->isenabler + (irq / 32)) |= maskIrqID;
}

/**
 * Allows to enable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void a9_gicc_irq_enable(unsigned  int irq) {
	a9_gicc_irq_unmask(irq);
}

/**
 * Allows to disable an interrupt.
 *
 * @param irq the interrupt ID
 */
static void a9_gicc_irq_disable(unsigned  int irq) {
	a9_gicc_irq_mask(irq);
}

/**
 * Allows to handle an interrupt.
 */
static void a9_gicc_irq_handle(cpu_regs_t *cpu_regs) {
	uint32_t irqstat;
	uint32_t irqnr;

	do {
		/* Get the interrupt ID. */
		irqstat = a9_gicc_dev->iar;
		irqnr   = irqstat & GICC_IAR_INT_ID_MASK;

		/* The interrupt is a SPI. */
		if (irqnr > 31 && irqnr < NB_IRQS) {
			irq_process(irqnr);

			a9_gicc_dev->eoir = irqstat;
		}
		/* The interrupt is a SGI. */
		else if (irqnr < 32) {
			a9_gicc_dev->eoir = irqstat;
		}
	/* When there is no more irq to handle, `irqnr` = 1023. */
	} while (irqnr < NB_IRQS);
}


/**
 * Initializes the GIC CPU interface.
 */
static int a9_gicc_init(dev_t *dev) {
	DBG("Begin init GIC CPU interface...\n");

	a9_gicc_dev = (a9_gicc_t *)dev->base;

	/* Set the priority filter. */
	a9_gicc_dev->pmr = GICC_INT_PRI_THRESHOLD;

	/* Enable GICC. */
	a9_gicc_dev->ctlr = GICC_ENABLE;

	/* Make the link between the GIC and the structure that allows to handle */
	/* the interrupts. */
	irq_ops.irq_enable  = a9_gicc_irq_enable;
	irq_ops.irq_disable = a9_gicc_irq_disable;
	irq_ops.irq_mask    = a9_gicc_irq_mask;
	irq_ops.irq_unmask  = a9_gicc_irq_unmask;
	irq_ops.irq_handle  = a9_gicc_irq_handle;

	DBG("End init GICC...\n");

	return 0;
}

/**
 * Initializes the GIC distributor.
 */
static int a9_gicd_init(dev_t *dev) {
	unsigned int i;
	uint32_t cpuMask;

	DBG("Begin init GIC distributor...\n");

	a9_gicd_dev = (a9_gicd_t *)dev->base;

	/* first of all we disable the GIC distributor */
	a9_gicd_dev->ctlr = GICD_DISABLE;

	/* prepare the CPU 0 mask to set it as target for all SPIs */
	cpuMask   = GICD_CPU_0_MASK;
	cpuMask  |= cpuMask << 8;
	cpuMask  |= cpuMask << 16;

	/* set all SPI interrupts to the target CPU 0 */
	for (i = 32; i < NB_IRQS; i += 4) {
		*(a9_gicd_dev->itargetsr + i / 4) = cpuMask;
	}

	/* configure the distributor */
	a9_gicd_config();
	a9_gicc_config();

	/* finally we enable the GIC distributor */
	a9_gicd_dev->ctlr = GICD_ENABLE;

	DBG("End init GICD...\n");

	return 0;
}


REGISTER_DRIVER_CORE("arm,cortex-a9-gicd", a9_gicd_init);
REGISTER_DRIVER_CORE("arm,cortex-a9-gicc", a9_gicc_init);
