/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <thread.h>

int fn1(void *args) {
	int i = 0;

	printk("Thread #1\n");

	while (1) {
		printk("--> th 1: %d\n", i++);
	}

	return 0;
}

int fn2(void *args) {
	int i = 0;

	printk("Thread #1\n");
	while (1) {
		printk("--> th 2: %d\n", i++);
	}

	return 0;
}

/*
 * Main entry point of so3 app in kernel standalone configuration.
 * Mainly for debugging purposes.
 */
int main_kernel(void *args) {

	/* Kernel never returns ! */

	printk("***********************************************\n");
	printk("Going to infinite loop...\n");
	printk("Kill Qemu with CTRL-a + x or reset the board\n");
	printk("***********************************************\n");

	kernel_thread(fn1, "fn1", NULL, 0);
	kernel_thread(fn2, "fn2", NULL, 0);


	return 0;
}

