 /*
 * Copyright (C) 2017 Lucas Elisei <lucas.elisei@heig-vd.ch>
 * Copyright (C) 2017 David Truan <david.truan@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef DW_APB_H
#define DW_APB_H

#include <types.h>

#define UART_THR		0x0

/* UART clock frequency (100 MHz) */
#define UART_CLK_FREQ	100000000

/* UART baud rate */
#define	UART_BAUDRATE	115200

/* Data Length for LCR register */
#define UART_DL_5BITS	0x0
#define UART_DL_6BITS	0x1
#define UART_DL_7BITS	0x2
#define UART_DL_8BITS	0x3

/* LCR register */
#define UART_LCR_STOP_1		(0 << 2)
#define UART_LCR_STOP_1_5	(1 << 2)
#define UART_LCR_PDIS		(0 << 3)
#define UART_LCR_PEN		(1 << 3)
#define	UART_LCR_DLAB		(1 << 7)

/* MCR register */
#define UART_MCR_DTR		(1 << 0)
#define UART_MCR_RTS		(1 << 1)

/* LSR register */
#define UART_LSR			0x14
#define UART_LSR_DR			(1 << 0)
#define UART_LSR_THRE		(1 << 5)

typedef struct {
	/* 0x00 */
	union {
		volatile uint32_t rbr;
		volatile uint32_t thr;
		volatile uint32_t dll;
	};
	/* 0x04 */
	union {
		volatile uint32_t dlh;
		volatile uint32_t ier;
	};
	/* 0x08 */
	union {
		volatile uint32_t iir;
		volatile uint32_t fcr;
	};
	volatile uint32_t lcr;			// 0x0C
	volatile uint32_t mcr;			// 0x10
	volatile uint32_t lsr;			// 0x14
	volatile uint32_t msr;			// 0x18
	volatile uint32_t scr;			// 0x1C
	volatile uint32_t lpdll;		// 0x20
	volatile uint32_t lpdlh;		// 0x24
	volatile uint32_t _res0[2];		// 0x28 - 0x2C
	/* 0x30 */
	union {
		volatile uint32_t srbr;
		volatile uint32_t sthr;
		volatile uint32_t _res1[14];
	};
	volatile uint32_t far;			// 0x70
	volatile uint32_t tfr;			// 0x74
	volatile uint32_t rfw;			// 0x78
	volatile uint32_t usr;			// 0x7C
	volatile uint32_t tfl;			// 0x80
	volatile uint32_t rfl;			// 0x84
	volatile uint32_t srr;			// 0x88
	volatile uint32_t srts;			// 0x8C
	volatile uint32_t sbcr;			// 0x90
	volatile uint32_t sdmam;		// 0x94
	volatile uint32_t sfe;			// 0x98
	volatile uint32_t srt;			// 0x9C
	volatile uint32_t stet;			// 0xA0
	volatile uint32_t htx;			// 0xA4
	volatile uint32_t dmasa;		// 0xA8
	volatile uint32_t _res2[18];	// 0xAC - 0xF0
	volatile uint32_t cpr;			// 0xF4
	volatile uint32_t ucv;			// 0xF8
	volatile uint32_t ctr;			// 0xFC
} dw_apb_t;

#endif /* DW_APB_H */
