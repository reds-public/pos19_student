 /*
 * Copyright (C) 2017 Lucas Elisei <lucas.elisei@heig-vd.ch>
 * Copyright (C) 2017 David Truan <david.truan@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */


#ifndef DW_APB_TIMER_H_
#define DW_APB_TIMER_H_

// Maximum value for the timer counter register.
#define MAX_COUNTER_VALUE	(uint32_t)(0xFFFFFFFF)

// Maximum value converted to nanoseconds.
#define MAX_TIME_NS			MICROSECS(MAX_COUNTER_VALUE)

#define TIMER_CTRL_ENABLE	(1 << 0)
#define TIMER_CTRL_FREE		(0 << 1)
#define TIMER_CTRL_PERIODIC	(1 << 1)
#define TIMER_CTRL_IE		(0 << 2)

typedef struct {
	/* 0x00: Load register */
	volatile uint32_t load;
	/* 0x04: Counter register */
	volatile uint32_t counter;
	/* 0x08: Control register */
	volatile uint32_t control;
	/* 0x0C: End-of-Interrupt register */
	volatile uint32_t eoi;
	/* 0x10: Interrupt status register */
	volatile uint32_t is;
} dw_apb_timer_t;

#endif /* DW_APB_TIMER_H_ */
