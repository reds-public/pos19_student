/*
 * gic_socfpga.h
 *
 *  Created on: Nov 23, 2017
 *      Author: Lucas Elisei & David Truan
 * Description: GIC registers and defines for Cortex A9 GIC.
 */

#ifndef INCLUDE_DEVICE_ARCH_ARM_A9_GIC_H_
#define INCLUDE_DEVICE_ARCH_ARM_A9_GIC_H_


#include <types.h>
#include <device/device.h>

/* Total number of IRQ sources handled by the controller
 *   0 - 15  are SGI interrupts: SGI = Software Generated Interrupts
 *  16 - 31  are PPI interrupts: PPI = Private Peripheral Interrupts
 *  32 - 256 are SPI interrupts: SPI = Shared Peripheral Interrupts
 */
#define NB_IRQS					256

/* constants to handles GIC distributor */
#define GICD_ENABLE				0x0001
#define GICD_DISABLE			0x0000
#define GICD_CPU_0_MASK			0x01
#define GICD_MAX_NB_IRQS		1020
#define GICD_INT_ACTLOW_LVLTRIG	0x0000
#define GICD_INT_DEF_PRI		0xa0
#define GICD_INT_DEF_PRI_X4		((GICD_INT_DEF_PRI << 24) |\
								 (GICD_INT_DEF_PRI << 16) |\
								 (GICD_INT_DEF_PRI << 8) |\
								  GICD_INT_DEF_PRI)
#define GICD_INT_EN_CLR_X32		0xffffffff
#define GICD_INT_EN_SET_SGI		0x0000ffff
#define GICD_INT_EN_CLR_PPI		0xffff0000

#define GIC_CPU_CTRL			0x00
#define GIC_CPU_PRIMASK			0x04
#define GIC_CPU_BINPOINT		0x08
#define GIC_CPU_INTACK			0x0c
#define GIC_CPU_EOI				0x10
#define GIC_CPU_RUNNINGPRI		0x14
#define GIC_CPU_HIGHPRI			0x18
#define GIC_CPU_ALIAS_BINPOINT	0x1c
#define GIC_CPU_ACTIVEPRIO		0xd0
#define GIC_CPU_IDENT			0xfc

#define GICC_ENABLE				0x1
#define GICC_INT_PRI_THRESHOLD	0xf0
#define GICC_IAR_INT_ID_MASK	0x3ff
#define GICC_INT_SPURIOUS		1023

/* Structure that represents the GIC controller. */
typedef struct {
	/* 0x00 */
	volatile uint32_t ctlr;
	/* 0x04 */
	volatile uint32_t pmr;
	/* 0x08 */
	volatile uint32_t _res;
	/* 0x0C */
	volatile uint32_t iar;
	/* 0x10 */
	volatile uint32_t eoir;
} a9_gicc_t;

/* Structure that represents the GIC distributor. */
typedef struct {
	/* 0x00 */
	volatile uint32_t ctlr;
	/* 0x04 - 0x96 */
	volatile uint32_t _res1[63];
	/* 0x100 */
	volatile uint32_t isenabler[32];
	/* 0x180 */
	volatile uint32_t icenabler[32];
	/* 0x200 - 0x3FC */
	volatile uint32_t _res2[128];
	/* 0x400 */
	volatile uint32_t ipriorityr[256];
	/* 0x800 */
	volatile uint32_t itargetsr[256];
	/* 0xC00 */
	volatile uint32_t icfgr[64];
} a9_gicd_t;

#endif /* INCLUDE_DEVICE_ARCH_ARM_A9_GIC_H_ */